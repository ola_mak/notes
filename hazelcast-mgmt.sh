set -x
# konfiguracja START
export JAVA_HOME=/usr/lib/jvm/java-8-adoptopenjdk/
env=local_am
SERVER_IP=192.168.0.101
PLATFORM_HOME=/home/users/amak/Projekty/mailbox/poczta2-platform
# konfiguracja STOP

CONTAINER_POSTFIX_JOB=local
PORT_OFFSET_JOB=0
STARTUP_HOME=$PLATFORM_HOME/poczta2-platform-distribution/target/poczta2-platform-distribution-20203.3.0-SNAPSHOT
cd $PLATFORM_HOME && mvn clean package -U
cd $PLATFORM_HOME/poczta2-platform-distribution/target && unzip *.zip

mkdir -p ~/storage/local1
mkdir -p ~/storage/local2
mkdir -p ~/storage/local3
mkdir -p ~/storage/hazelcast_mgmt

# zatrzymanie kontenerów
cd $STARTUP_HOME && bash run.sh $env stop hazelcast-mgmt
cd $STARTUP_HOME && bash run.sh $env stop hazelcast PORT_OFFSET=$((PORT_OFFSET_JOB)) CONTAINER_POSTFIX=${CONTAINER_POSTFIX_JOB}1 HAZELCAST_HOST=${SERVER_IP}
cd $STARTUP_HOME && bash run.sh $env stop hazelcast PORT_OFFSET=$((PORT_OFFSET_JOB+1)) CONTAINER_POSTFIX=${CONTAINER_POSTFIX_JOB}2 HAZELCAST_HOST=${SERVER_IP}
cd $STARTUP_HOME && bash run.sh $env stop hazelcast PORT_OFFSET=$((PORT_OFFSET_JOB+2)) CONTAINER_POSTFIX=${CONTAINER_POSTFIX_JOB}3 HAZELCAST_HOST=${SERVER_IP}

# budowanie obrazów 
cd $STARTUP_HOME && bash run.sh $env build hazelcast-mgmt


cd $STARTUP_HOME && bash run.sh $env build hazelcast PORT_OFFSET=$((PORT_OFFSET_JOB+2)) CONTAINER_POSTFIX=${CONTAINER_POSTFIX_JOB}3 HAZELCAST_HOST=${SERVER_IP}

# wystartowanie
cd $STARTUP_HOME && bash run.sh $env build hazelcast PORT_OFFSET=$((PORT_OFFSET_JOB)) CONTAINER_POSTFIX=${CONTAINER_POSTFIX_JOB}1 HAZELCAST_HOST=${SERVER_IP}
cd $STARTUP_HOME && bash run.sh $env start hazelcast PORT_OFFSET=$((PORT_OFFSET_JOB)) CONTAINER_POSTFIX=${CONTAINER_POSTFIX_JOB}1 HAZELCAST_HOST=${SERVER_IP}
cd $STARTUP_HOME && bash run.sh $env build hazelcast PORT_OFFSET=$((PORT_OFFSET_JOB+1)) CONTAINER_POSTFIX=${CONTAINER_POSTFIX_JOB}2 HAZELCAST_HOST=${SERVER_IP}
cd $STARTUP_HOME && bash run.sh $env start hazelcast PORT_OFFSET=$((PORT_OFFSET_JOB+1)) CONTAINER_POSTFIX=${CONTAINER_POSTFIX_JOB}2 HAZELCAST_HOST=${SERVER_IP}
cd $STARTUP_HOME && bash run.sh $env build hazelcast PORT_OFFSET=$((PORT_OFFSET_JOB+2)) CONTAINER_POSTFIX=${CONTAINER_POSTFIX_JOB}3 HAZELCAST_HOST=${SERVER_IP}
cd $STARTUP_HOME && bash run.sh $env start hazelcast PORT_OFFSET=$((PORT_OFFSET_JOB+2)) CONTAINER_POSTFIX=${CONTAINER_POSTFIX_JOB}3 HAZELCAST_HOST=${SERVER_IP}
cd $STARTUP_HOME && bash run.sh $env start hazelcast-mgmt